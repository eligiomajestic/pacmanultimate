﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Lib.Entidades;
using Microsoft.Xna.Framework;

namespace Lib.Sistema
{
    public class Mapa
    {
        public bool amarilla = true;
        public int escalaX = 32;
        public int escalaY = 32;
        public Random randX = new Random((int)System.DateTime.Now.Ticks);
        public Random randY = new Random();
        public int[,] matriz;
        public Color Colorante = Color.White;
        public static string mapName = "Level1.txt";
        public float Tiempo;
        public List<PowerUp>listaDePowerUp;
        int numeroMapa;

        public static bool Ganador;

        public Mapa(int numeroDelMapa)
        {
            mapName = "Level" + numeroDelMapa + ".txt"; 
            matriz = new int[25, 20];
            LlenarMatrizDesdeTxt();
            numeroMapa = numeroDelMapa;
            Ganador = VerificarGanar();
            
            IniciarlizadorDeListas();

            if (numeroDelMapa == 2)
                Colorante = Color.Tomato;
            else if (numeroDelMapa == 3)
                Colorante = Color.SteelBlue;
            else if (numeroDelMapa == 4)
                Colorante = Color.Yellow;
        }

        public void IniciarlizadorDeListas()
        {
            listaDePowerUp = new List<PowerUp>();
        }

        public void Pintate()
        {
            //pintamos bloques recoriendo la matriz
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    if (matriz[j, i] == 1)
                        Juego.spriteBatch.Draw(Juego.TexturaBlock, new Rectangle(j * escalaX, i * escalaY, Juego.TexturaBlock.Width, Juego.TexturaBlock.Height), null, Colorante, 0f, Vector2.Zero, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                    else if (matriz[j, i] == 2)
                        Juego.spriteBatch.Draw(Juego.TexturaPunto, new Rectangle((j * escalaX) + 14, (i * escalaY) + 14, Juego.TexturaPunto.Width, Juego.TexturaPunto.Height), null, Color.White, 0f, Vector2.Zero, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                    else if (matriz[j, i] == 3)
                        Juego.spriteBatch.Draw(Juego.TexturaPunto, new Rectangle((j * escalaX) + 14, (i * escalaY) + 14, Juego.TexturaPunto.Width, Juego.TexturaPunto.Height), null, Color.White, 0f, Vector2.Zero, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                    else if (matriz[j, i] == 4)
                        Juego.spriteBatch.Draw(Juego.TexturaPuntoGrande, new Rectangle((j * escalaX) + 14, (i * escalaY) + 14, Juego.TexturaPuntoGrande.Width, Juego.TexturaPuntoGrande.Height), null, Color.White, 0f, Vector2.Zero, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                }
            }

            //pintamos powers up
            foreach (PowerUp x in listaDePowerUp)
            {
                if (x.activo)
                {
                    x.Pintar(Juego.spriteBatch);
                }
            }


            var color = Juego.Pacman.colorante;
            var color2 = Juego.Pacman2.colorante;
            if (Juego.Pacman.vidas == 0)
                color = Color.Gray;
            if (Juego.Pacman2.vidas == 0)
                color2 = Color.Gray;

            if (Juego.cantidadDeJugadores == 1)
            {
                Juego.spriteBatch.DrawString(Juego.font, ("Score: " + Juego.Pacman.puntos.ToString()), new Vector2(0, 640), Juego.Pacman.colorante);
                Juego.spriteBatch.Draw(Juego.texturaPlayer1, new Vector2(180, 640), null, Color.White, 0f, Vector2.Zero, 1f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                Juego.spriteBatch.DrawString(Juego.font, ("x" + Juego.Pacman.vidas.ToString()), new Vector2(210, 640), color);
                
            }

            if (Juego.cantidadDeJugadores == 2)
            {
                Juego.spriteBatch.DrawString(Juego.font, ("Score: " + Juego.Pacman.puntos.ToString()), new Vector2(0, 640), Juego.Pacman.colorante);
                Juego.spriteBatch.Draw(Juego.texturaPlayer1, new Vector2(180, 640), null, Color.White, 0f, Vector2.Zero, 1f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                Juego.spriteBatch.DrawString(Juego.font, ("x" + Juego.Pacman.vidas.ToString()), new Vector2(215, 640), color);
                Juego.spriteBatch.DrawString(Juego.font, ("Score: " + Juego.Pacman2.puntos.ToString()), new Vector2(0, 670), Juego.Pacman2.colorante);
                Juego.spriteBatch.Draw(Juego.texturaPlayer2, new Vector2(180, 670), null, Color.White, 0f, Vector2.Zero, 1f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1f);
                Juego.spriteBatch.DrawString(Juego.font, ("x" + Juego.Pacman2.vidas.ToString()), new Vector2(215, 670), color2);
            }
        }

        public void Actualizate(GameTime dataTime) 
        {
            int randomX = randX.Next(0, 100);
            if(randomX == 11)
                IntentaCrearPowerUp();

            //Actualizamos powers up
            foreach (PowerUp x in listaDePowerUp)
            {
                if (x.activo)
                {
                    x.Actualizar(dataTime);
                }
            }
        }

        public void LlenarMatrizDesdeTxt()
        {
            StreamReader reader = new StreamReader("Content/Archivos/"+mapName); { 
                char[] letras = new char[15]; 
                string linea; 
                for (int j = 0; j < 20; j++) { 
                    linea = reader.ReadLine(); 
                    letras = linea.ToCharArray(); 
                    for (int i = 0; i < letras.Length; i++) 
                    {
                        if (letras[i] == 'x')
                            matriz[i, j] = 1;
                        else if (letras[i] == 'i')
                            matriz[i, j] = 2;
                        else if (letras[i] == 'p')
                            matriz[i, j] = 3;
                        else if (letras[i] == 'k')
                            matriz[i, j] = 4;

                    }
                }
            }
        }

        public bool VerificarGanar()
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    if (matriz[j, i] == 2)
                        return false;
                }
            }
            return true;

        }

        public void IntentaCrearPowerUp()
        {
            Color Coolorante = Color.White;
            if (Juego.modoJuego == eModoDeJuego.Competitivo)
            {
                if (amarilla)
                    Coolorante = Color.Yellow;
                else
                    Coolorante = Color.Red;
            }

            int randomX = randX.Next(0, 25);
            int randomY = randY.Next(0, 20);
          
            if (matriz[randomX, randomY] != 1 && (listaDePowerUp.Count <= 3) && (numeroMapa == 1))
            {
                listaDePowerUp.Add(new PowerUp(eTipoPowerUp.ScorePlus, new Vector2((randomX * escalaX) + 14, (randomY * escalaY) + 14), Juego.TexturaPowerUp, Coolorante));
                if (Juego.modoJuego == eModoDeJuego.Competitivo)
                {
                    if (amarilla)
                        amarilla = false;
                    else
                        amarilla = true;
                }
            }
            else if (matriz[randomX, randomY] != 1 && (listaDePowerUp.Count <= 3) && (numeroMapa == 2))
            {
                listaDePowerUp.Add(new PowerUp(eTipoPowerUp.ScorePlus, new Vector2((randomX * escalaX) + 10, (randomY * escalaY) + 10), Juego.TexturaPowerUp2, Coolorante));
                if (Juego.modoJuego == eModoDeJuego.Competitivo)
                {
                    if (amarilla)
                        amarilla = false;
                    else
                        amarilla = true;
                }
            }
            else if (matriz[randomX, randomY] != 1 && (listaDePowerUp.Count <= 3) && (numeroMapa == 3))
            {
                listaDePowerUp.Add(new PowerUp(eTipoPowerUp.ScorePlus, new Vector2((randomX * escalaX) + 10, (randomY * escalaY) + 10), Juego.TexturaPowerUp3, Coolorante));
                if (Juego.modoJuego == eModoDeJuego.Competitivo)
                {
                    if (amarilla)
                        amarilla = false;
                    else
                        amarilla = true;
                }
            }
            else if (matriz[randomX, randomY] != 1 && (listaDePowerUp.Count <= 3) && (numeroMapa == 4))
            {
                listaDePowerUp.Add(new PowerUp(eTipoPowerUp.ScorePlus, new Vector2((randomX * escalaX) + 10, (randomY * escalaY) + 10), Juego.TexturaPowerUp4, Coolorante));
                if (Juego.modoJuego == eModoDeJuego.Competitivo)
                {
                    if (amarilla)
                        amarilla = false;
                    else
                        amarilla = true;
                }
            }

        }
    }
}
