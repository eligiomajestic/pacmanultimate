﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Entidades;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lib.Sistema
{
    public enum eTipoPowerUp { ScorePlus }
    public class PowerUp : Objeto
    {
        public static eTipoPowerUp tipo;

        public PowerUp(eTipoPowerUp Tipo, Vector2 Posicion, Texture2D Textura, Color Colorante ) 
        {
            colorante = Colorante;
            tipo = Tipo;
            posicion = Posicion;
            textura = Textura;
        }

    }
}
