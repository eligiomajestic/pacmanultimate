using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Entidades;
using Lib.Sistema;
using Lib.Recursos;

namespace Lib
{

    public enum eEstadoJuego { Menu, Jugando, SeleccionarMundo, Ganar}
    public enum eModoDeJuego { Competitivo, Cooperativo}

    public static class Juego
    {
        public static Save salvadoReitentar = new Save("save.xml");
        public static GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        public static KeyboardState teclado;
        public static MouseState mouse;
        public static GamePadState control1;
        public static GamePadState control2;
        public static Song cancionInicial;
        public static Song sonidoInicioNivel;
        public static SoundEffect sonidoComiendo;
        public static SoundEffectInstance instance;
        public static SoundEffect sonidoMuerte;

        public static int estandarDeTamano = 32;
        public static int cantidadDeJugadores = 1;


        public static bool ningunMenuActivo = false;

        #region "Estado del Juego"
        public static eEstadoJuego estadoJuego = eEstadoJuego.Menu;
        public static eModoDeJuego modoJuego = eModoDeJuego.Cooperativo;
        #endregion

        #region "Mapa"
        public static Mapa mapa1;
        public static Mapa mapa2;
        public static Mapa mapa3;
        public static Mapa mapa4;

        public static Mapa mapaActual;
        #endregion

        #region "personajes"
        public static Heroe Pacman = new Heroe(Color.Yellow);
        public static Heroe Pacman2 = new Heroe(Color.Red);
        public static Heroe tempPacman = new Heroe(Color.White);
        public static Heroe tempPacman2 = new Heroe(Color.Red);

        public static List<Enemigo> listaEnemigos = new List<Enemigo>();
        #endregion

        #region "spritefonts"
        public static SpriteFont font;
        #endregion

        #region "Texturas"
        public static Dictionary<String, String> dictTexturasYMusica;
        public static Texture2D TexturaPacmanArriba;
        public static Texture2D TexturaPacmanAbajo;
        public static Texture2D TexturaPacmanDerecha;
        public static Texture2D TexturaPacmanIzquierda;
        public static Texture2D TexturaPacmanMuerte;
        public static Texture2D TexturaFantasma;
        public static Texture2D TexturaFantasmaMuerte;

        public static Texture2D TexturaBlock;
        public static Texture2D TexturaImanAmarillo;
        public static Texture2D TexturaImanRojo;
        public static Texture2D texturaPlayer1;
        public static Texture2D texturaPlayer2;
        public static Texture2D texturaPacmanSolo;

        public static Texture2D TexturaPunto;
        public static Texture2D TexturaPuntoGrande;
        public static Texture2D TexturaPowerUp;
        public static Texture2D TexturaPowerUp2;
        public static Texture2D TexturaPowerUp3;
        public static Texture2D TexturaPowerUp4;

        #endregion

        #region "Funciones"
        public static void ActualizarVariables() // esto se ejecuta de priero
        {
            teclado = Keyboard.GetState();
            mouse = Mouse.GetState();
            control1 = GamePad.GetState(PlayerIndex.One);
            control2 = GamePad.GetState(PlayerIndex.Two);

        }

        public static void CargarEnemigos() 
        {
            listaEnemigos.Add(new Enemigo((Color.White), new Vector2(10 * 32, 9 * 32), listaEnemigos.Count + 1));
            listaEnemigos.Add(new Enemigo((Color.Red), new Vector2(11 * 32, 9 * 32), listaEnemigos.Count + 1));
            listaEnemigos[1].Inteligencia = 2;
            listaEnemigos.Add(new Enemigo((Color.Yellow), new Vector2(13 * 32, 9 * 32), listaEnemigos.Count + 1));
            listaEnemigos[2].Inteligencia = 3;
            listaEnemigos.Add(new Enemigo((Color.Green), new Vector2(14 * 32, 9 * 32), listaEnemigos.Count + 1));
            listaEnemigos[3].Inteligencia = 5;
        }
        public static void CargarTexturasYMusica(ContentManager manager) 
        {
            dictTexturasYMusica = manager.Load<Dictionary<String, String>>("Archivos/RutasTexturas");
            TexturaPacmanArriba = manager.Load<Texture2D>(dictTexturasYMusica["PacmanArriba"]);
            TexturaPacmanAbajo = manager.Load<Texture2D>(dictTexturasYMusica["PacmanAbajo"]);
            TexturaPacmanDerecha = manager.Load<Texture2D>(dictTexturasYMusica["PacmanDerecha"]);
            TexturaPacmanIzquierda = manager.Load<Texture2D>(dictTexturasYMusica["PacmanIzquierda"]);
            TexturaPacmanMuerte = manager.Load<Texture2D>(dictTexturasYMusica["PacmanMuerte"]);

            TexturaFantasma = manager.Load<Texture2D>(dictTexturasYMusica["Fantasma"]);
            TexturaFantasmaMuerte = manager.Load<Texture2D>(dictTexturasYMusica["FantasmaMuerte"]);
            TexturaBlock = manager.Load<Texture2D>(dictTexturasYMusica["Bloque"]);
            TexturaPuntoGrande = manager.Load<Texture2D>(dictTexturasYMusica["PuntoGrande"]);
            TexturaPowerUp = manager.Load<Texture2D>(dictTexturasYMusica["Manzana"]);
            TexturaPowerUp2 = manager.Load<Texture2D>(dictTexturasYMusica["Pera"]);
            TexturaPowerUp3 = manager.Load<Texture2D>(dictTexturasYMusica["Banana"]);
            TexturaPowerUp4 = manager.Load<Texture2D>(dictTexturasYMusica["Cereza"]);
            TexturaPunto = manager.Load<Texture2D>(dictTexturasYMusica["Punto"]);
            TexturaImanAmarillo = manager.Load<Texture2D>(dictTexturasYMusica["imanAmarillo"]);
            TexturaImanRojo = manager.Load<Texture2D>(dictTexturasYMusica["imanRojo"]);
            texturaPlayer1 = manager.Load<Texture2D>(dictTexturasYMusica["imanRojo"]);
            texturaPlayer2 = manager.Load<Texture2D>(dictTexturasYMusica["imanRojo"]);
            texturaPacmanSolo = manager.Load<Texture2D>(dictTexturasYMusica["PacmanSolo"]);

            cancionInicial = manager.Load<Song>(dictTexturasYMusica["CancionInicial"]);
            sonidoComiendo = manager.Load<SoundEffect>(dictTexturasYMusica["SonidoComiendo"]);
            sonidoInicioNivel = manager.Load<Song>(dictTexturasYMusica["SonidoInicioNivel"]);
            sonidoMuerte = manager.Load<SoundEffect>(dictTexturasYMusica["Muerto"]);
            instance = manager.Load<SoundEffect>(dictTexturasYMusica["SonidoComiendo"]).CreateInstance();
            
            font = manager.Load<SpriteFont>(dictTexturasYMusica["PrimerFont"]);
            

        }
        public static void CargarAnimaciones() 
        {
            Pacman.CargarAnimaciones();
            Pacman2.CargarAnimaciones();
            tempPacman.CargarAnimaciones();
            tempPacman2.CargarAnimaciones();
        }
        public static void PintarPersonajes() 
        {
            Pacman.Pintar(spriteBatch);
            if (cantidadDeJugadores == 2) 
            {
                Pacman2.Pintar(spriteBatch);
            }

            foreach (Enemigo i in listaEnemigos)
            {
                i.Pintar(spriteBatch);
            }
        }

        public static void ActualizarPersonajes(GameTime dataTime) 
        {
            Pacman.Actualizar(dataTime);
            if (cantidadDeJugadores == 2) 
            {
                Pacman2.Actualizar(dataTime);
            }
            if(estadoJuego == eEstadoJuego.Jugando)
            {
                foreach (Enemigo i in listaEnemigos)
                {
                    i.Actualizar(dataTime);
                }
            }
        }

        public static void MoverPersonajes() 
        {
            // pueba 
            if (teclado.IsKeyDown(Keys.T))
            {
                Juego.estadoJuego = eEstadoJuego.Ganar;
            }
            else if (teclado.IsKeyDown(Keys.R))
                Juego.salvadoReitentar.Cargar();


            GamePadState temp = new GamePadState();

                temp = control1;
              
                if (teclado.IsKeyDown(Keys.Left))
                {
                    Pacman.MoverIzquierda();
                }
                else if (teclado.IsKeyDown(Keys.Right))
                {
                    Pacman.MoverDerecha();
                }
                else if (teclado.IsKeyDown(Keys.Up))
                {
                    Pacman.MoverArriba();
                }
                else if (teclado.IsKeyDown(Keys.Down))
                {
                    Pacman.MoverAbajo();
                }

                Pacman.index = PlayerIndex.One;

            if (cantidadDeJugadores == 2)
            {
                temp = control2;
                if (teclado.IsKeyDown(Keys.A) ||
                (temp.IsConnected && temp.DPad.Left == ButtonState.Pressed) ||
                temp.ThumbSticks.Left.X < -0.1f)
                {
                    Pacman2.MoverIzquierda();
                }
                else if (teclado.IsKeyDown(Keys.D) ||
                (temp.IsConnected && temp.DPad.Right == ButtonState.Pressed) ||
                temp.ThumbSticks.Left.X > 0.1f)
                {
                    Pacman2.MoverDerecha();
                }
                else if (teclado.IsKeyDown(Keys.W) ||
                (temp.IsConnected && temp.DPad.Up == ButtonState.Pressed) ||
                temp.ThumbSticks.Left.Y > 0.1f)
                {
                    Pacman2.MoverArriba();
                }
                else if (teclado.IsKeyDown(Keys.S) ||
                (temp.IsConnected && temp.DPad.Down == ButtonState.Pressed) ||
                temp.ThumbSticks.Left.Y < -0.1f)
                {
                    Pacman2.MoverAbajo();
                }
                Pacman2.index = PlayerIndex.Two;
            }
        }

        public static void CambiarEstadoJuego()
        {
           if (estadoJuego == eEstadoJuego.Menu && teclado.IsKeyDown(Keys.Enter))
            {
                estadoJuego = eEstadoJuego.SeleccionarMundo;
            }
           else if (estadoJuego == eEstadoJuego.SeleccionarMundo && teclado.IsKeyDown(Keys.A))
           {
               estadoJuego = eEstadoJuego.Jugando;
               salvadoReitentar.Guardar();
           }
        }

        public static void CargarMapas() 
        {
            mapa1 = new Mapa(1);
            mapa2 = new Mapa(2);
            mapa3 = new Mapa(3);
            mapa4 = new Mapa(4);

            mapaActual = mapa1;
        }


        #endregion
    }
}
