﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using Lib.Entidades;
using Microsoft.Xna.Framework;
using Lib.Sistema;

namespace Lib.Recursos
{
    [Serializable]
    public class Save
    {
        public string Nombre;
        Heroe p1 = new Heroe(Color.Yellow);
        Heroe p2 = new Heroe(Color.Red);


        public Save() { }
        public Save(string name) 
        {
            Nombre = name;
        }

        public void Guardar()
        {
            //cargamos
            p1 = Juego.Pacman;
            p2 = Juego.Pacman2;


            using (FileStream archivo = new FileStream(Nombre, FileMode.Create))
            {
                XmlSerializer serializador = new XmlSerializer(typeof(Save));
                serializador.Serialize(archivo, this);
            }
        }

        public void Cargar()
        {
            using (FileStream archivo = new FileStream(Nombre, FileMode.Open))
            {
                
                XmlSerializer Deserializador = new XmlSerializer(typeof(Save));
                Save temp = (Save)Deserializador.Deserialize(archivo);

                // aqui cargamos el juego
                Juego.Pacman.posicion = temp.p1.posicion;
                Juego.Pacman.puntos = temp.p1.puntos;
                Juego.Pacman.vidas = temp.p1.vidas;

                Juego.Pacman2.posicion = temp.p2.posicion;
                Juego.Pacman2.puntos = temp.p2.puntos;
                Juego.Pacman2.vidas = temp.p2.vidas;

                Juego.listaEnemigos = new List<Enemigo>();
                Juego.CargarEnemigos();

                if (Juego.mapaActual == Juego.mapa1) 
                {
                    Juego.mapaActual = new Mapa(1);
                    Juego.mapa1 = Juego.mapaActual;
                }
                else if (Juego.mapaActual == Juego.mapa2)
                {
                    Juego.mapaActual = new Mapa(2);
                    Juego.mapa2 = Juego.mapaActual;
                }
                else if (Juego.mapaActual == Juego.mapa3)
                {
                    Juego.mapaActual = new Mapa(3);
                    Juego.mapa3 = Juego.mapaActual;
                }
                else if (Juego.mapaActual == Juego.mapa4)
                {
                    Juego.mapaActual = new Mapa(4);
                    Juego.mapa4 = Juego.mapaActual;
                }
            }
        }
    }
}
