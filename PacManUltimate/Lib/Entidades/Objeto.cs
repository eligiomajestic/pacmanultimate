﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Lib.Entidades
{
    public class Objeto
    {
        public bool activo = true;
        public float escala = 1f;
        public Texture2D textura;
        public Vector2 posicion;
        public Color colorante = Color.White;
        public Vector2 tamano;
        public Rectangle rectangulo;

        public virtual void Actualizar(GameTime dataTime)
        {
            rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, (int)tamano.X, (int)tamano.Y);
        }

        public virtual void Pintar(SpriteBatch batch)
        {
            if (activo)
            {
                batch.Draw(textura, new Rectangle((int)posicion.X, (int)posicion.Y, textura.Width, textura.Height), colorante);
            }
        }

        #region "Funciones"
        public bool ColisionConMigo(Rectangle rectangulo2) 
        {
            if (rectangulo.Intersects(rectangulo2))
                return true;
            else
                return false;
        }
        #endregion
    }
}
