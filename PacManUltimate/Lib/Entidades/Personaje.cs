﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;
using Lib.Sistema;

namespace Lib.Entidades
{
    public enum eDireccion { Arriba, Abajo, Derecha, Izquierda, Ninguna}

    public class Personaje : Objeto
    {
        public Animacion aniArriba;
        public Animacion aniAbajo;
        public Animacion aniDerecha;
        public Animacion aniIzquierda;
        public Animacion aniActual;
        public Animacion aniMuerte;

        public PlayerIndex index;

        public eDireccion enuDireccion = eDireccion.Ninguna;
        public eDireccion enuColisionX = eDireccion.Ninguna;
        public eDireccion enuColisionY = eDireccion.Ninguna;

        public Vector2 velocidad;

        public float rapidez = 50f;

        public float profundidad = 0f;
        public bool Vivo = true;
        public bool Muriendo = false;
        public bool dentroDePantalla = true;

        public override void Actualizar(GameTime dataTime)
        {
            posicion += velocidad * (float)dataTime.ElapsedGameTime.TotalSeconds;
            rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, (int)tamano.X, (int)tamano.Y);
            VerificarDentroDePantalla();
            ColisionConMapa(Juego.mapaActual.matriz);
            EfectoEspejo();

           
            //Aqui pones sus animaciones corespondientes
            #region "animaciones selecion y actualizacion"

            Animacion();
            aniActual.ponerPosicion(posicion.X, posicion.Y);
            aniActual.Escala = escala;


            if (enuDireccion != eDireccion.Ninguna) //aqui dejamos de actulizar la animacion si no se esta moviendo y arriba la pusimos en el frame cero
                aniActual.Actualizar(dataTime, true);
            #endregion

        }
        public override void Pintar(SpriteBatch batch)
        {
            if (Vivo)
            {
                aniActual.Dibujar(batch, profundidad, colorante);
            }
        }

        #region "Movimiento"`   
        public void MoverDerecha()
        {
            if (dentroDePantalla)
            {
                if (Juego.mapaActual.matriz[1 + ((int)((posicion.X + tamano.X / 2)) / Juego.estandarDeTamano), (int)((posicion.Y) / Juego.estandarDeTamano)] != 1 && Juego.mapaActual.matriz[1 + ((int)((posicion.X + tamano.X / 2)) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y) / Juego.estandarDeTamano)] != 1)
                {
                    if (enuDireccion != eDireccion.Derecha && escala <= 1 && enuColisionX != eDireccion.Derecha)
                    {
                        velocidad = new Vector2(rapidez, 0);
                        enuDireccion = eDireccion.Derecha;
                    }
                }
            }
        }
        public void MoverIzquierda()
        {
            if (dentroDePantalla)
            {
                if (Juego.mapaActual.matriz[((int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano)) - 1, (int)((posicion.Y) / Juego.estandarDeTamano)] != 1 && Juego.mapaActual.matriz[((int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano)) - 1, (int)((posicion.Y + tamano.Y) / Juego.estandarDeTamano)] != 1 )
                {
                    if (enuDireccion != eDireccion.Izquierda && escala <= 1 && enuColisionX != eDireccion.Izquierda)
                    {
                        velocidad = new Vector2(-rapidez, 0);
                        enuDireccion = eDireccion.Izquierda;
                    }
                }
            }
        }
        public void MoverArriba()
        {
            if (dentroDePantalla)
            {
                if (Juego.mapaActual.matriz[(int)((posicion.X) / Juego.estandarDeTamano), ((int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)) - 1] != 1 && Juego.mapaActual.matriz[(int)((posicion.X + tamano.X) / Juego.estandarDeTamano), ((int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)) - 1] != 1)
                {
                    if (enuDireccion != eDireccion.Arriba && escala <= 1 && enuColisionY != eDireccion.Arriba && dentroDePantalla)
                    {
                        velocidad = new Vector2(0, -rapidez);
                        enuDireccion = eDireccion.Arriba;
                    }
                }
            }
        }
        public void MoverAbajo()
        {
            if (dentroDePantalla)
            {
                if (Juego.mapaActual.matriz[(int)((posicion.X) / Juego.estandarDeTamano), ((int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)) + 1] != 1 && Juego.mapaActual.matriz[(int)((posicion.X + tamano.X) / Juego.estandarDeTamano), ((int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)) + 1] != 1)
                {
                    if (enuDireccion != eDireccion.Abajo && escala <= 1 && enuColisionY != eDireccion.Abajo && dentroDePantalla)
                    {
                        velocidad = new Vector2(0, rapidez);
                        enuDireccion = eDireccion.Abajo;
                    }
                }
            }
        }
        public void Saltar(Point Destino) 
        {

        }
        #endregion

        public void EfectoEspejo() 
        {
            if (posicion.X > 830)
                posicion.X = -30;
            else if (posicion.X < -30)
                posicion.X = 830;
            if (posicion.Y > 670)
                posicion.Y = -30;
            else if (posicion.Y < -30)
                posicion.Y = 670;
        }
        public void ColisionConMapa(int[,] matriz)  //diagrama de esto se compueban 2 puntos de cada lado
        {
            int rango = 5;
            if (dentroDePantalla)
            {
                if (matriz[(int)((posicion.X - rango) / Juego.estandarDeTamano), (int)((posicion.Y) / Juego.estandarDeTamano)] == 1 || matriz[(int)((posicion.X - rango) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y) / Juego.estandarDeTamano)] == 1)
                {
                    enuColisionX = eDireccion.Izquierda;
                    if (velocidad.X < 0)
                        velocidad.X = 0;
                }
                else if (matriz[(int)((posicion.X + tamano.X + rango) / Juego.estandarDeTamano), (int)((posicion.Y) / Juego.estandarDeTamano)] == 1 || matriz[(int)((posicion.X + tamano.X + rango) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y) / Juego.estandarDeTamano)] == 1)
                {
                    enuColisionX = eDireccion.Derecha;
                    if (velocidad.X > 0)
                        velocidad.X = 0;
                }
                else
                {
                    enuColisionX = eDireccion.Ninguna;
                }

                if (matriz[(int)((posicion.X) / Juego.estandarDeTamano), (int)((posicion.Y - rango) / Juego.estandarDeTamano)] == 1 || matriz[(int)((posicion.X + tamano.X) / Juego.estandarDeTamano), (int)((posicion.Y - rango) / Juego.estandarDeTamano)] == 1)
                {
                    enuColisionY = eDireccion.Arriba;
                    if (velocidad.Y < 0)
                        velocidad.Y = 0;
                }
                else if (matriz[(int)((posicion.X) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y + rango) / Juego.estandarDeTamano)] == 1 || matriz[(int)((posicion.X + tamano.X) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y + rango) / Juego.estandarDeTamano)] == 1)
                {
                    enuColisionY = eDireccion.Abajo;
                    if (velocidad.Y > 0)
                        velocidad.Y = 0;
                }
                else
                {
                    enuColisionY = eDireccion.Ninguna;
                }
            }
            else 
            {
                enuColisionX = eDireccion.Ninguna;
                enuColisionY = eDireccion.Ninguna;
            }
        }
        public void VerificarDentroDePantalla() 
        {
            int rango = 27;
            if (posicion.X < rango || posicion.X > 800 - tamano.X - rango || posicion.Y < rango || posicion.Y > 640 - tamano.Y - rango)
                dentroDePantalla = false;
            else
                dentroDePantalla = true;
        }

        public void Animacion()
        {
            if (!Muriendo)
            {
                if (enuDireccion == eDireccion.Derecha)
                    aniActual = aniDerecha;
                else if (enuDireccion == eDireccion.Izquierda)
                    aniActual = aniIzquierda;
                else if (enuDireccion == eDireccion.Abajo)
                    aniActual = aniAbajo;
                else if (enuDireccion == eDireccion.Arriba)
                    aniActual = aniArriba;
                else if (enuDireccion == eDireccion.Ninguna)
                    aniActual.frames = 0;
                if (velocidad == Vector2.Zero)
                    enuDireccion = eDireccion.Ninguna;
            }
            else
                aniActual = aniMuerte;
        }

    }
}
