﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Lib.Sistema;
using Microsoft.Xna.Framework.Media;

namespace Lib.Entidades
{
    public class Heroe : Personaje
    {
        public int puntos = 0;
        public int vidas = 3;



        public Heroe(Color Colorante)
        {
            posicion = new Vector2(32,32);
            profundidad = 0f;
            enuDireccion = eDireccion.Derecha;
            rapidez = 100;
            tamano = new Vector2(26, 26);
            colorante = Colorante;
        }

        #region"funciones"
        public override void Actualizar(GameTime dataTime)
        {
            if (Vivo && !Muriendo)
            {
                base.Actualizar(dataTime); //aqui estamos llamando al actualizar de personaje que es general

                ColisionConListaDePuntos(Juego.mapaActual.matriz);

                ColisionEnemigos(Juego.listaEnemigos);
                ColisionPowerUps();
            }
            else if (Muriendo)
            {
                Animacion();
                aniActual.Actualizar(dataTime, true);
                aniActual.ponerPosicion(posicion.X, posicion.Y);
                if (aniActual.frames == 19)
                {
                    Muere();
                    Muriendo = false;
                    aniActual.frames = 0;
                }
            }

        }

        #region "Animaciones"
        public void CargarAnimaciones() 
        {
            aniAbajo = new Recursos.Animacion(Juego.graphics , posicion);
            aniAbajo.adicionarImagen(Juego.TexturaPacmanAbajo, 3);
            aniAbajo.iniciar(0, 2, 0.1f, true);

            aniArriba = new Recursos.Animacion(Juego.graphics, posicion);
            aniArriba.adicionarImagen(Juego.TexturaPacmanArriba, 3);
            aniArriba.iniciar(0, 2, 0.1f, true);

            aniDerecha = new Recursos.Animacion(Juego.graphics, posicion);
            aniDerecha.adicionarImagen(Juego.TexturaPacmanDerecha, 3);
            aniDerecha.iniciar(0, 2, 0.1f, true);

            aniIzquierda = new Recursos.Animacion(Juego.graphics, posicion);
            aniIzquierda.adicionarImagen(Juego.TexturaPacmanIzquierda, 3);
            aniIzquierda.iniciar(0, 2, 0.1f, true);

            aniMuerte = new Recursos.Animacion(Juego.graphics, posicion);
            aniMuerte.adicionarImagen(Juego.TexturaPacmanMuerte, 20);
            aniMuerte.iniciar(0, 19, 0.7f, false);

            aniActual = aniDerecha;
        }
        #endregion
        public void ColisionConListaDePuntos(int[,] matriz) 
        {
            if (dentroDePantalla)
            {
                if (matriz[(int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)] == 2)
                {
                    puntos += 5;
                    matriz[(int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)] = 0;
                    if (Juego.estadoJuego == eEstadoJuego.Jugando)
                    {
                        Juego.instance.Pause();
                        Juego.instance.Play();
                    }  
                }
                if (matriz[(int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)] == 4)
                {
                    puntos += 5;
                    matriz[(int)((posicion.X + tamano.X / 2) / Juego.estandarDeTamano), (int)((posicion.Y + tamano.Y / 2) / Juego.estandarDeTamano)] = 0;
                    if (Juego.estadoJuego == eEstadoJuego.Jugando)
                    {
                        foreach (Enemigo i in Juego.listaEnemigos)
                        {
                            i.Estado = eEstadoEnemigo.Asustado;
                        }
                    }
                }
            }
            
        }

        public void ColisionPowerUps()
        {
            foreach (PowerUp i in Juego.mapaActual.listaDePowerUp)
            {
                if (i.ColisionConMigo(rectangulo) && (i.colorante == colorante || i.colorante == Color.White))
                {
                    i.activo = false;
                    i.rectangulo = Rectangle.Empty;
                    puntos += 20;
                    Juego.instance.Pause();
                    Juego.instance.Play();
                }

            }
        }
        public void ColisionEnemigos(List<Enemigo> lista)
        {
            foreach (Enemigo i in lista)
            {
                if (i.rectangulo.Intersects(rectangulo))
                {
                    if (i.Estado == eEstadoEnemigo.Asustado)
                    {
                        i.Muriendo = true;
                    }
                    else
                    {
                        Juego.sonidoMuerte.Play();
                        Muriendo = true;
                        
                    }
                }
            }
        }

        public void Muere() 
        {
            if (vidas > 0)
            {
                posicion = new Vector2(36, 36);
                vidas--;
            }
            else
                Vivo = false;
           
        }

        #endregion
    }
}
