﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Lib.Entidades
{
    public enum eEstadoEnemigo { Asustado, Normal, Muerto}
    public class Enemigo : Personaje
    {
        public int id;
        public eEstadoEnemigo Estado = eEstadoEnemigo.Normal;
        public bool Controlado = false;
        public int Inteligencia = 1;
        public Color ColoranteOriginal;
        public float Tiempo;

        public Enemigo(Color Colorante, Vector2 Posicion, int ID)
        {
            Estado = eEstadoEnemigo.Normal;
            rapidez = 100;
            id = ID;
            colorante = Colorante;
            ColoranteOriginal = Colorante;
            posicion = Posicion;
            Tiempo = 0;

            CargarAnimaciones();
            tamano = new Vector2(26, 26);
        }

        public override void Actualizar(GameTime dataTime)
        {
            
            if (!Muriendo)
            {
                base.Actualizar(dataTime); //aqui estamos llamando al actualizar de personaje que es general

                aniActual.ponerPosicion(posicion.X - 5, posicion.Y);
                if (!Controlado)
                {
                    IA();
                }
                else
                    PlayerControl();

                if (Estado == eEstadoEnemigo.Asustado)
                {
                    Tiempo += (float)dataTime.ElapsedGameTime.TotalSeconds;
                    colorante = Color.Blue;
                }
                else
                    colorante = ColoranteOriginal;
                
                if (Estado == eEstadoEnemigo.Asustado && (Tiempo >= 7.0f))
                {
                    Estado = eEstadoEnemigo.Normal;
                    Tiempo = 0;
                }    
            }


            else if (Muriendo)
            {

                Animacion();
                aniActual.Actualizar(dataTime, true);
                aniActual.ponerPosicion(posicion.X, posicion.Y);
                if (aniActual.frames == 7)
                {
                    Muere();
                    Muriendo = false;
                    aniActual.frames = 0;
                    Tiempo = 0;
                }
            }
        }

        #region "Funciones"

        #region "Animaciones"
        public void CargarAnimaciones()
        {
            aniAbajo = new Recursos.Animacion(Juego.graphics, posicion);
            aniAbajo.adicionarImagen(Juego.TexturaFantasma, 8);
            aniAbajo.iniciar(4, 5, 0.2f, true);

            aniArriba = new Recursos.Animacion(Juego.graphics, posicion);
            aniArriba.adicionarImagen(Juego.TexturaFantasma, 8);
            aniArriba.iniciar(0, 1, 0.2f, true);

            aniDerecha = new Recursos.Animacion(Juego.graphics, posicion);
            aniDerecha.adicionarImagen(Juego.TexturaFantasma, 8);
            aniDerecha.iniciar(6, 7, 0.2f, true);

            aniIzquierda = new Recursos.Animacion(Juego.graphics, posicion);
            aniIzquierda.adicionarImagen(Juego.TexturaFantasma, 8);
            aniIzquierda.iniciar(2, 3, 0.2f, true);

            aniMuerte = new Recursos.Animacion(Juego.graphics, posicion);
            aniMuerte.adicionarImagen(Juego.TexturaFantasmaMuerte, 8);
            aniMuerte.iniciar(0, 7, 0.2f, true);

            aniActual = aniArriba;

        }
        #endregion

        public void IA()
        {
            //dentro de caja 
            if (posicion.Y > 8 * 32 && posicion.Y < 10 * 32)
            {
                if (enuDireccion != eDireccion.Arriba)
                {
                    if (posicion.X > (12 * 32) + 5)
                        MoverIzquierda();
                    else
                        MoverDerecha();
                }
            }
           

            Random rand = new Random(System.DateTime.Now.Millisecond);
            if (rand.Next(0,12) > 4) // para que no doble en todas las esquinas
            {
                if (rand.Next(0 , 8 - Inteligencia) < 2)
                {
                    Persiguelo();
                }
                else
                    Alocado();
            }
        }

        public void Persiguelo() 
        {

            if (enuDireccion == eDireccion.Abajo || enuDireccion == eDireccion.Arriba) 
            {
                if (Juego.Pacman.posicion.X > posicion.X)
                    MoverDerecha();
                else
                    MoverIzquierda();
            }
            else if (enuDireccion == eDireccion.Derecha || enuDireccion == eDireccion.Izquierda) 
            {
                if (Juego.Pacman.posicion.Y > posicion.Y)
                    MoverAbajo();
                else
                    MoverArriba();
            }
            else 
            {
                Alocado();
            }
        }
        public void Alocado() 
        {
            Random rand = new Random(System.DateTime.Now.Millisecond);
            if (enuDireccion == eDireccion.Abajo || enuDireccion == eDireccion.Arriba)
            {
                if (rand.Next(1, 10) > 4)
                    MoverDerecha();
                else
                    MoverIzquierda();
            }
            else if (enuDireccion == eDireccion.Derecha || enuDireccion == eDireccion.Izquierda)
            {
                if (rand.Next(1, 10) > 4)
                    MoverAbajo();
                else
                    MoverArriba();
            }
            else
                {
                    int num = rand.Next(1, 40);
                    if (num <= 10)
                        MoverDerecha();
                    else if (num <= 20)
                        MoverIzquierda();
                    else if (num <= 30)
                        MoverArriba();
                    else
                        MoverAbajo();
                }
        }
        public void PlayerControl() 
        {
            if (Juego.teclado.IsKeyDown(Keys.Delete))
            {
                MoverIzquierda();
            }
            else if (Juego.teclado.IsKeyDown(Keys.PageDown))
            {
                MoverDerecha();
            }
            else if (Juego.teclado.IsKeyDown(Keys.Home))
            {
                MoverArriba();
            }
            else if (Juego.teclado.IsKeyDown(Keys.End))
            {
                MoverAbajo();
            }
        }

        public void Muere()
        {
            posicion = new Vector2(320,290);
            rectangulo = Rectangle.Empty;
            Muriendo = false;
            Estado = eEstadoEnemigo.Normal;

        }
        #endregion


    }

   
}
