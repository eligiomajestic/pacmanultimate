using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace PacManUltimate
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Menu : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch batch;

        SpriteFont font;
        SpriteFont font2;


        float cambiaColor = 0.2f;
        float TiempoActual;

        Texture2D texturaMenu;
        Texture2D texturaBarra;

        public Menu(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
 
            base.Initialize();
        }


        protected override void LoadContent()
        {
            texturaMenu = Game.Content.Load<Texture2D>("ImagenesSistema/picpac");
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            font2 = Game.Content.Load<SpriteFont>("Archivos/textoMenu2");
            texturaBarra = Game.Content.Load<Texture2D>("ImagenesSistema/bar1");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            TiempoActual += gameTime.ElapsedGameTime.Milliseconds;
            if (TiempoActual > 100)
            {
                if (cambiaColor < 1f)
                {
                    cambiaColor += 0.5f;
                }
                else if(cambiaColor >= 0.2f)
                    cambiaColor -= 0.5f;

                TiempoActual = 0;
            }

            batch.Begin();

            batch.Draw(texturaMenu, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(texturaBarra, new Vector2(305, 310), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Presione Enter Para Continuar", new Vector2(320, 320), Color.Yellow * cambiaColor);
            batch.DrawString(font2, "P", new Vector2(60, 5), Color.Red);
            batch.DrawString(font2, "A", new Vector2(140, 5), Color.Orange);
            batch.DrawString(font2, "C", new Vector2(250, 5), Color.LightBlue);
            batch.DrawString(font2, "M", new Vector2(340, 5), Color.Yellow);
            batch.DrawString(font2, "A", new Vector2(470, 5), Color.Purple);
            batch.DrawString(font2, "N", new Vector2(600, 5), Color.YellowGreen);
            batch.DrawString(font2, "U", new Vector2(10, 500), Color.YellowGreen);
            batch.DrawString(font2, "L", new Vector2(110, 500), Color.Yellow);
            batch.DrawString(font2, "T", new Vector2(180, 500), Color.Orange);
            batch.DrawString(font2, "I", new Vector2(280, 500), Color.LightBlue);
            batch.DrawString(font2, "M", new Vector2(340, 500), Color.Orchid);
            batch.DrawString(font2, "A", new Vector2(460, 500), Color.Purple);
            batch.DrawString(font2, "T", new Vector2(560, 500), Color.Lime);
            batch.DrawString(font2, "E", new Vector2(660, 500), Color.Red);
            batch.End();
            
            base.Draw(gameTime);
        }
    }
}
