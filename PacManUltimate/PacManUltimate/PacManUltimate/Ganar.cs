using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;


namespace PacManUltimate
{

    public class Ganar : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        SpriteFont font;

        public Vector2 aceleracionPacman;
        public Vector2 aceleracionPacman2;

        Texture2D texturaPlayer1;
        Texture2D texturaPlayer2;
        bool semaforo = false;

        public Ganar(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            Random rand = new Random((int)System.DateTime.Now.Ticks);

            Juego.tempPacman.velocidad.X = rand.Next(5, 10) * 10;
            aceleracionPacman.X = rand.Next(5, 10) * -1;
            aceleracionPacman.Y = 10;

            Juego.tempPacman2.velocidad.X = rand.Next(1, 5) * 10;
            aceleracionPacman2.X = rand.Next(5, 10);
            aceleracionPacman2.Y = 10;

            Juego.tempPacman.posicion = new Vector2(580,0);
            Juego.tempPacman2.posicion = new Vector2(0, 150);
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu3");
            texturaPlayer1 = Game.Content.Load<Texture2D>("ImagenesSistema/pacman1icon");
            texturaPlayer2 = Game.Content.Load<Texture2D>("ImagenesSistema/pacman2icon");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Juego.tempPacman.rectangulo = new Rectangle((int)Juego.tempPacman.posicion.X, (int)Juego.tempPacman.posicion.Y, Juego.texturaPacmanSolo.Width, Juego.texturaPacmanSolo.Height);
            Juego.tempPacman2.rectangulo = new Rectangle((int)Juego.tempPacman2.posicion.X, (int)Juego.tempPacman2.posicion.Y, Juego.texturaPacmanSolo.Width, Juego.texturaPacmanSolo.Height);

            Juego.tempPacman.velocidad += aceleracionPacman;
            Juego.tempPacman.posicion += new Vector2((float)(Juego.tempPacman.velocidad.X * gameTime.ElapsedGameTime.TotalSeconds),(float)(Juego.tempPacman.velocidad.Y * gameTime.ElapsedGameTime.TotalSeconds));
            Juego.tempPacman.aniActual.ponerPosicion(Juego.tempPacman.posicion.X, Juego.tempPacman.posicion.Y);

            Juego.tempPacman2.velocidad += aceleracionPacman2;
            Juego.tempPacman2.posicion += new Vector2((float)(Juego.tempPacman2.velocidad.X * gameTime.ElapsedGameTime.TotalSeconds), (float)(Juego.tempPacman2.velocidad.Y * gameTime.ElapsedGameTime.TotalSeconds));
            Juego.tempPacman2.aniActual.ponerPosicion(Juego.tempPacman2.posicion.X, Juego.tempPacman2.posicion.Y);

            Rebote(Juego.tempPacman);
            Rebote(Juego.tempPacman2);

            if (IntersectPixels(Juego.tempPacman.rectangulo, Juego.texturaPacmanSolo, Juego.tempPacman2.rectangulo, Juego.texturaPacmanSolo))
            {
                if (!semaforo)
                {
                    Juego.tempPacman.velocidad.X *= -1;
                    Juego.tempPacman2.velocidad.X *= -1;
                    semaforo = true;
                }
            }
            else
                semaforo = false;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.Black);
            batch.Begin();
            batch.DrawString(font, "WINNER!", new Vector2(330, 300), Color.White);
            batch.Draw(Juego.TexturaImanRojo,new Rectangle(800 - Juego.TexturaImanRojo.Width/2,0,Juego.TexturaImanRojo.Width,Juego.TexturaImanRojo.Height),Color.White);
            batch.Draw(Juego.TexturaImanAmarillo, new Rectangle(0 - Juego.TexturaImanAmarillo.Width / 2, 0, Juego.TexturaImanAmarillo.Width, Juego.TexturaImanAmarillo.Height), Color.White);

            if (Juego.modoJuego == eModoDeJuego.Cooperativo) 
            {
                batch.Draw(texturaPlayer1, new Vector2(300, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                batch.DrawString(font, "P1!", new Vector2(300, 340), Color.Yellow);
                batch.DrawString(font, Juego.Pacman.puntos.ToString(), new Vector2(300, 420), Color.Yellow);

                if (Juego.cantidadDeJugadores == 2)
                {
                    batch.Draw(texturaPlayer2, new Vector2(465, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    batch.DrawString(font, "P2!", new Vector2(465, 340), Color.Red);
                    batch.DrawString(font, Juego.Pacman2.puntos.ToString(), new Vector2(465, 420), Color.Red);
                }
            }
            if (Juego.modoJuego == eModoDeJuego.Competitivo)
            {
                if (Juego.Pacman.puntos > Juego.Pacman2.puntos)
                {
                    batch.Draw(texturaPlayer1, new Vector2(300, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    batch.DrawString(font, "P1!", new Vector2(300, 340), Color.Yellow);
                    batch.DrawString(font, Juego.Pacman.puntos.ToString(), new Vector2(300, 420), Color.Yellow);
                }
                else if (Juego.Pacman2.puntos > Juego.Pacman.puntos)
                {
                    batch.Draw(texturaPlayer2, new Vector2(465, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    batch.DrawString(font, "P2!", new Vector2(465, 340), Color.Red);
                    batch.DrawString(font, Juego.Pacman2.puntos.ToString(), new Vector2(465, 420), Color.Red);
                }
                else if (Juego.Pacman2.puntos == Juego.Pacman.puntos)
                {
                    batch.Draw(texturaPlayer1, new Vector2(300, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    batch.DrawString(font, "P1!", new Vector2(300, 340), Color.Yellow);
                    batch.DrawString(font, Juego.Pacman.puntos.ToString(), new Vector2(300, 420), Color.Yellow);
                    batch.Draw(texturaPlayer2, new Vector2(465, 380), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    batch.DrawString(font, "P2!", new Vector2(465, 340), Color.Red);
                    batch.DrawString(font, Juego.Pacman2.puntos.ToString(), new Vector2(465, 420), Color.Red);

                }
                

            }
            Juego.tempPacman.Pintar(batch);
            Juego.tempPacman2.Pintar(batch);
            batch.End();
            base.Draw(gameTime);
        }

        public void Rebote(Heroe temp) 
        {
            if(temp.posicion.X > 780)
            {
                if (temp.velocidad.X > 0)
                    temp.velocidad.X *= -1;
            }
            else if (temp.posicion.X < 28)
            {
                if (temp.velocidad.X < 0)
                    temp.velocidad.X *= -1;
            }
            if (temp.posicion.Y > 700)
            {
                if (temp.velocidad.Y > 0)
                    temp.velocidad.Y *= -1;
            }
            else if (temp.posicion.Y < 28)
            {
                if (temp.velocidad.Y < 0)
                    temp.velocidad.Y *= -1;
            }
        }

        public bool IntersectPixels(Rectangle rectangleA, Texture2D textureA, Rectangle rectangleB, Texture2D textureB) {

            //Encontrar limites de la interseccion de los dos rectangulos
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);
            Color[] dataA = new Color[textureA.Width * textureA.Height];
            Color[] dataB = new Color[textureB.Width * textureB.Height];
            textureA.GetData(dataA);
            textureB.GetData(dataB);

            //Verificar cada pixel de esa interseccion
            for (int y = top; y < bottom; y++) {
                for (int x = left; x < right; x++) {
                    //Obtiene el color de ambos pixeles en esta posicion
                    Color colorA = dataA[(x - rectangleA.Left) +
                                         (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) +
                                         (y - rectangleB.Top) * rectangleB.Width];

                    //Si los dos pixeles no son completamente transparentes
                    if (colorA.A != 0 && colorB.A != 0) {

                        //Entonces ha habido una colision
                        return true;
                    }
                }
            }

            //No hubo interseccion
            return false;
        }
    }
}
