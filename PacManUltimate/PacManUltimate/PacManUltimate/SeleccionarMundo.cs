using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;

namespace PacManUltimate
{
    public enum eSeleccionMundoX { Derecha, Izquierda }
    public enum eSeleccionMundoY { Arriba, Abajo }
    public enum eSeleccionJugador { P1 = 1, P2 }
    public enum eSeleccionModo { Cooperativo = 1, Competitivo }

    public class SeleccionarMundo : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;

        public static eSeleccionMundoX seleccionMundo = eSeleccionMundoX.Derecha;
        public static eSeleccionMundoY seleccionMundoY = eSeleccionMundoY.Arriba;
        public static eSeleccionJugador seleccionJugador = eSeleccionJugador.P1;
        public static eSeleccionModo seleccionModo = eSeleccionModo.Cooperativo;

        SpriteFont font2;

        public static Texture2D texturaMundo;
        public static Texture2D texturaMundo2;
        public static Texture2D texturaMundo3;
        public static Texture2D texturaMundo4;

        Texture2D texturaBarra;
        Texture2D texturaMarcoJugador;
        Texture2D texturaMarcoModo;

        public SeleccionarMundo(Game game)
            : base(game)
        {
            
        }

        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();

            base.Initialize();
        }

        protected override void LoadContent()
        {

            texturaMundo = Game.Content.Load<Texture2D>("ImagenesSistema/Mundo1");
            texturaMundo2 = Game.Content.Load<Texture2D>("ImagenesSistema/Mundo2");
            texturaMundo3 = Game.Content.Load<Texture2D>("ImagenesSistema/Mundo3");
            texturaMundo4 = Game.Content.Load<Texture2D>("ImagenesSistema/Mundo4");

            texturaBarra = Game.Content.Load<Texture2D>("ImagenesSistema/barmundo");
            texturaMarcoJugador = Game.Content.Load<Texture2D>("ImagenesSistema/marcoplayer");
            texturaMarcoModo = Game.Content.Load<Texture2D>("ImagenesSistema/marcoModo");
            Juego.texturaPlayer1 = Game.Content.Load<Texture2D>("ImagenesSistema/pacman1icon");
            Juego.texturaPlayer2 = Game.Content.Load<Texture2D>("ImagenesSistema/pacman2icon");
            font2 = Game.Content.Load<SpriteFont>("Archivos/textoMenu3");
            base.LoadContent();
        }
 

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Right))
                seleccionMundo = eSeleccionMundoX.Izquierda;
            if (ks.IsKeyDown(Keys.Left))
                seleccionMundo = eSeleccionMundoX.Derecha;
            if (ks.IsKeyDown(Keys.Down))
                seleccionMundoY = eSeleccionMundoY.Abajo;
            if (ks.IsKeyDown(Keys.Up))
                seleccionMundoY = eSeleccionMundoY.Arriba;
            


            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            batch.Begin();
            
            batch.Draw(texturaBarra, new Vector2((((int)(seleccionMundo) * 320) + 60), (((int)(seleccionMundoY) * 259) + 30)), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(texturaMundo, new Vector2(70, 40), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(texturaMundo2, new Vector2(390, 40), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(texturaMundo3, new Vector2(70, 300), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(texturaMundo4, new Vector2(390, 300), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(texturaMarcoJugador, new Vector2((((int)(seleccionJugador) * 60)),  590), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(texturaMarcoModo, new Vector2((((int)(seleccionModo) * texturaMarcoModo.Width-50)+(int)(seleccionModo-1)*50), 650), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(Juego.texturaPlayer1, new Vector2(70, 600), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(Juego.texturaPlayer2, new Vector2(135, 600), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font2, "Cooperativo", new Vector2(140, 653), Color.Yellow);
            batch.DrawString(font2, "Competitivo", new Vector2(380, 653), Color.Yellow);
            batch.End();
            base.Draw(gameTime);
        }
    }
}
