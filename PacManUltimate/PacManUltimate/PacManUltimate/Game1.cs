using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;
using Lib.Sistema;

namespace PacManUltimate
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        Menu menu;
        SeleccionarMundo selMun;
        Ganar gana;
        public Game1()
        {
            Juego.graphics = new GraphicsDeviceManager(this);
            Juego.graphics.PreferredBackBufferWidth = 800;
            Juego.graphics.PreferredBackBufferHeight = 740;
            Juego.graphics.IsFullScreen = false;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;
            menu = new Menu(this);
            menu.Enabled = false;
            menu.Initialize();

            selMun = new SeleccionarMundo(this);
            selMun.Enabled = false;
            selMun.Initialize();

            gana = new Ganar(this);
            gana.Enabled = false;
            gana.Initialize();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            Juego.spriteBatch = new SpriteBatch(GraphicsDevice);

            Juego.CargarTexturasYMusica(Content);

            Juego.CargarMapas();
            Juego.CargarEnemigos();

            Juego.CargarAnimaciones(); // esto tiene que ir luego de crear los personajes para que no d error
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            Juego.ActualizarVariables();
            Juego.ActualizarPersonajes(gameTime);
            if (Juego.ningunMenuActivo)
            {
                Juego.MoverPersonajes();
                if (Juego.mapaActual.VerificarGanar())
                    Juego.estadoJuego = eEstadoJuego.Ganar;

                //Actualizamos el mapa
                Juego.mapaActual.Actualizate(gameTime);
            }
            Juego.CambiarEstadoJuego();
            ManejadorDeComponentes();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
           GraphicsDevice.Clear(Color.Black);

            Juego.spriteBatch.Begin();

            Juego.PintarPersonajes();

            Juego.mapaActual.Pintate();

            Juego.spriteBatch.End();
            base.Draw(gameTime);
        }

        public void ManejadorDeComponentes()
        {
            switch (Juego.estadoJuego)
            {
                case eEstadoJuego.Jugando:
                    if (selMun.Enabled)
                    {
                        MediaPlayer.Play(Juego.sonidoInicioNivel);
                        MediaPlayer.IsRepeating = false;
                        Components.Remove(selMun);
                        selMun.Enabled = false;
                    }
                    Juego.ningunMenuActivo = true;


                    break;
                case eEstadoJuego.Ganar:
                    if (!gana.Enabled)
                    {
                        Components.Add(gana);
                        gana.Enabled = true;
                    }
                    break;

                case eEstadoJuego.Menu:
                    if (!menu.Enabled)
                    {
                        MediaPlayer.Play(Juego.cancionInicial);
                        MediaPlayer.IsRepeating = true;
                        Components.Add(menu);
                        menu.Enabled = true;
                    }
                    break;

                case eEstadoJuego.SeleccionarMundo:

                    Components.Remove(menu);
                    menu.Enabled = false;
                    if (!selMun.Enabled)
                    {
                        Components.Add(selMun);
                        selMun.Enabled = true;
                    }

                    //teoria para posible seleccion de mundos
                    if(SeleccionarMundo.seleccionMundo == eSeleccionMundoX.Derecha && SeleccionarMundo.seleccionMundoY == eSeleccionMundoY.Arriba)
                        Juego.mapaActual = Juego.mapa1;
                    if (SeleccionarMundo.seleccionMundo == eSeleccionMundoX.Izquierda && SeleccionarMundo.seleccionMundoY == eSeleccionMundoY.Arriba)
                        Juego.mapaActual = Juego.mapa2;
                    if (SeleccionarMundo.seleccionMundo == eSeleccionMundoX.Derecha && SeleccionarMundo.seleccionMundoY == eSeleccionMundoY.Abajo)
                        Juego.mapaActual = Juego.mapa3;
                    if (SeleccionarMundo.seleccionMundo == eSeleccionMundoX.Izquierda && SeleccionarMundo.seleccionMundoY == eSeleccionMundoY.Abajo)
                        Juego.mapaActual = Juego.mapa4;


                    if (Juego.mouse.X > 69 && Juego.mouse.X < 126)
                    {
                        if (Juego.mouse.Y > 600 && Juego.mouse.Y < 651)
                        {
                            if (Juego.mouse.LeftButton == ButtonState.Pressed)
                            {
                                SeleccionarMundo.seleccionJugador = eSeleccionJugador.P1;
                                Juego.cantidadDeJugadores = 1;
                            }
                        }
                    }

                    if (Juego.mouse.X > 127 && Juego.mouse.X < 184)
                    {
                        if (Juego.mouse.Y > 600 && Juego.mouse.Y < 651)
                        {
                            if (Juego.mouse.LeftButton == ButtonState.Pressed)
                            {
                                SeleccionarMundo.seleccionJugador = eSeleccionJugador.P2;
                                Juego.cantidadDeJugadores = 2;
                            }
                        }
                    }

                    if (Juego.mouse.X > 127 && Juego.mouse.X < 313)
                    {
                        if (Juego.mouse.Y > 651 && Juego.mouse.Y < 702)
                        {
                            if (Juego.mouse.LeftButton == ButtonState.Pressed)
                            {
                                SeleccionarMundo.seleccionModo = eSeleccionModo.Cooperativo;
                                Juego.modoJuego = eModoDeJuego.Cooperativo;
                            }
                        }
                    }

                    if (Juego.mouse.X > 374 && Juego.mouse.X < 560)
                    {
                        if (Juego.mouse.Y > 651 && Juego.mouse.Y < 702)
                        {
                            if (Juego.mouse.LeftButton == ButtonState.Pressed)
                            {
                                SeleccionarMundo.seleccionModo = eSeleccionModo.Competitivo;
                                Juego.modoJuego = eModoDeJuego.Competitivo;

                            }
                        }
                    }


                    break;
            }
        }
    }
}
